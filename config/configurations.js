const mongoose = require('mongoose');

function mongoconnect() {
  mongoose.connect('mongodb://s4tyansh:s4tyanshmongodb@ds259154.mlab.com:59154/bookkeeper_users', {
    useCreateIndex: true,
    useNewUrlParser: true
  });
}

module.exports = mongoconnect;
