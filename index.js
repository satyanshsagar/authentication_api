const express = require('express');
const bodyparser = require('body-parser');
const commonMiddlewares = require('./routes/common_middlewares/common').router;
const logger = require('./mylogger');
const userroute = require('./routes/user_routes/userroute');
const mongoconnect = require('./config/configurations');

const server = express();
mongoconnect();
server.use(bodyparser.json());
server.use(commonMiddlewares);
server.use(userroute);

server.use((error, req, res, next) => {
  res.writeHead(404, { 'content-type': 'text/plain' });
  res.end(`Error 404: ${error.message}`);
  next();
});

const myServer = server.listen(process.env.PORT || 3535, () => {
  logger(`server is live at address: http://localhost and port: ${myServer.address().port}`);
});

module.exports = server;
