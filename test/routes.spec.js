const expect = require('chai').expect;
const keygrip = require('keygrip');
const request = require('supertest');
const server = require('../index');

const cookie = Buffer.from(JSON.stringify({ username: 'test@gmail.com' })).toString('base64');
const kg = keygrip(['notsecret']);
const hash = kg.sign(`my-session=${cookie}`);
describe('Integration testing the HomePage', () => {
  it('it should return status ok', () => {
    request(server).get('/')
      .set('Accept', 'text/html')
      .set('cookie', [`my-session=${cookie}; my-session.sig=${hash};`])
      .then((response) => {
        expect(response.status).to.equal(200);
      });
  });
});

describe('Integration testing the Books page', () => {
  it('it should return status ok', () => {
    request(server).get('/books')
      .set('Accept', 'text/html')
      .set('cookie', [`my-session=${cookie}; my-session.sig=${hash};`])
      .then((response) => {
        expect(response.status).to.equal(200);
      });
  });
});

describe('Integration testing the author page', () => {
  it('it should return status ok', () => {
    request(server).get('/authors')
      .set('Accept', 'text/html')
      .set('cookie', [`my-session=${cookie}; my-session.sig=${hash};`])
      .then((response) => {
        expect(response.status).to.equal(200);
      });
  });
});

describe('Integration testing the Book route', () => {
  it('it should return status ok', () => {
    request(server).get('/books/21345')
      .set('Accept', 'text/html')
      .set('cookie', [`my-session=${cookie}; my-session.sig=${hash};`])
      .then((response) => {
        expect(response.status).to.equal(200);
      });
  });
  it('it should return Error', () => {
    request(server).get('/books/1')
      .set('Accept', 'text/html')
      .set('cookie', [`my-session=${cookie}; my-session.sig=${hash};`])
      .then((response) => {
        expect(response.status).to.equal(404);
      });
  });
});

describe('Integration testing the requests on author routes', () => {
  it('it should return status ok', () => {
    request(server).get('/authors/39')
      .set('Accept', 'text/html')
      .set('cookie', [`my-session=${cookie}; my-session.sig=${hash};`])
      .then((response) => {
        expect(response.status).to.equal(200);
      });
  });
  it('it should return Error', () => {
    request(server)
      .get('/authors/45')
      .set('Accept', 'text/html')
      .set('cookie', [`my-session=${cookie}; my-session.sig=${hash};`])
      .then((response) => {
        expect(response.status).to.equal(404);
      });
  });
});
