const expect = require('chai').expect;
const mocha = require('mocha');
const query = require('../../routes/books/bookcontroller');

const getbook = query.getbook;


mocha.describe('unit test for the book query', () => {
  mocha.it('book query should return invalid', async () => {
    expect(await getbook(20)).to.have.lengthOf(0);
  });
  mocha.it('checking getbook by id method', async () => {
    const book = await getbook(21345);
    expect(book).to.have.lengthOf(1);
    expect(book[0].isbn).to.equal(21345);
    expect(book[0].bookname).to.equal('Things Fall Apart');
    expect(book[0].imagelink).to.equal('images/things-fall-apart.jpg');
    expect(book[0].authorid).to.equal(39);
    expect(book[0].authorname).to.equal('Chinua Achebe');
  });
});
