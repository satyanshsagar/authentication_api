const expect = require('chai').expect;
const mocha = require('mocha');
const query = require('../../routes/authors/authorcontroller');

const details = { details: 'Chinua Achebe was a Nigerian novelist, poet, professor, and critic. His first novel Things Fall Apart, often considered his best, is the most widely read book in modern African literature. He won the Man Booker International Prize in 2007.' };
const getauthor = query.getauthor;

mocha.describe('unit test for the author query', () => {
  mocha.it('author query should return invalid', async () => {
    expect(await getauthor(20)).to.have.lengthOf(0);
  });
  mocha.it('author query should return valid', async () => {
    const author = await getauthor(39);
    expect(author).to.have.lengthOf(1);
    expect(author[0].authorname).to.equal('Chinua Achebe');
    expect(author[0].id).to.equal(39);
    expect(author[0].authordetails).to.equal(details.details);
  });
});
