const router = require('express').Router();
const usercontroller = require('./usercontroller');

router.post('/loginuser', async (req, res) => {
  console.log(req.body);
  const userdata = { email: req.body.email, password: req.body.password };
  const result = await usercontroller.authenticate(userdata);
  if (result === true) {
    res.send({user: true});
  } else {
    res.send({ user: false });
  }
});

router.post('/registeruser', async (req, res) => {
  const userdata = { email: req.body.email, password: req.body.password, username: req.body.username };
  const result = await usercontroller.register(userdata);
  console.log(result);
  if (result) {
    res.send({result:JSON.stringify(result)});
  } else {
    res.send({ result: false });
  }
});

module.exports = router;
