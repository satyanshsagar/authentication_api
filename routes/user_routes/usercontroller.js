const bcrypt = require('bcrypt');
const User = require('../../model/userschema');
const logger = require('../../mylogger');

async function authenticate(userdata) {
  let finalresult;
  try {
    const user = await User.find({ email: userdata.email });
    if (user.length > 0) {
      finalresult = await bcrypt.compare(userdata.password, user[0].password);
    } else {
      finalresult = false;
    }
  } catch (e) {
    logger(e.message);
  }
  return finalresult;
}

async function register(userdata) {
  let finalresult;
  try {
    const salt = bcrypt.genSaltSync(5);
    const hash = bcrypt.hashSync(userdata.password, salt);
    const newuser = User({
      username: userdata.username,
      email: userdata.email,
      password: hash
    });
    finalresult = await newuser.save();
  } catch (e) {
    logger(e.message);
  }
  return finalresult;
}

module.exports = {
  authenticate,
  register
};
